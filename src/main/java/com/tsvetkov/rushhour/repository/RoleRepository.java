package com.tsvetkov.rushhour.repository;

import com.tsvetkov.rushhour.domain.entity.Role;
import com.tsvetkov.rushhour.domain.entity.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(RoleName roleName);
}
