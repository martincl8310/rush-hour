package com.tsvetkov.rushhour.repository;

import com.tsvetkov.rushhour.domain.entity.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Integer> {
  List<Appointment> findAllByUserId(int userId);

  Optional<Appointment> findByIdAndUserId(int appointmentId, int userId);

}
