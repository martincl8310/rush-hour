package com.tsvetkov.rushhour.repository;

import com.tsvetkov.rushhour.domain.entity.Activity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ActivityRepository  extends JpaRepository<Activity, Integer> {
    Page<Activity> findAll(Pageable pageable);

    Optional<Activity> findByName(String activityName);

    Boolean existsById(int activityId);
}
