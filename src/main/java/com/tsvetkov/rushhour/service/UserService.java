package com.tsvetkov.rushhour.service;

import com.tsvetkov.rushhour.domain.entity.User;
import com.tsvetkov.rushhour.domain.model.binding.UserEditDto;
import com.tsvetkov.rushhour.domain.model.binding.UserPasswordEditDto;
import com.tsvetkov.rushhour.domain.model.binding.UserPersistDto;
import com.tsvetkov.rushhour.payload.LoginUserRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

  User persistUser(UserPersistDto userPersistDto);

  User findUserById(int userId);

  Page<User> findAllUsers(Pageable pageable);

  User editUser(int id, UserEditDto userDto);

  void deleteUser(int userId);

  User findUserByEmail(String email);

  void editUserPassword(int userId, UserPasswordEditDto userPasswordEditDto);

  void passwordValidation(LoginUserRequest loginUserRequest);
}
