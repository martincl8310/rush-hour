package com.tsvetkov.rushhour.service.impl;

import com.tsvetkov.rushhour.domain.entity.User;
import com.tsvetkov.rushhour.security.UserPrincipal;
import com.tsvetkov.rushhour.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserService userService;

    @Autowired
    public CustomUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User userByEmail = this.userService.findUserByEmail(email);
        return UserPrincipal.create(userByEmail);
    }

    public UserDetails loadUserById(int userId){
         User userById = this.userService.findUserById(userId);
         return UserPrincipal.create(userById);
    }

}
