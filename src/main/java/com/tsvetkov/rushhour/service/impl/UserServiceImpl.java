package com.tsvetkov.rushhour.service.impl;

import com.tsvetkov.rushhour.domain.entity.Role;
import com.tsvetkov.rushhour.domain.entity.RoleName;
import com.tsvetkov.rushhour.domain.entity.User;
import com.tsvetkov.rushhour.domain.model.binding.UserEditDto;
import com.tsvetkov.rushhour.domain.model.binding.UserPasswordEditDto;
import com.tsvetkov.rushhour.domain.model.binding.UserPersistDto;
import com.tsvetkov.rushhour.exception.customExeption.AccessDeniedException;
import com.tsvetkov.rushhour.exception.customExeption.ConflictException;
import com.tsvetkov.rushhour.exception.customExeption.EntityNotFoundException;
import com.tsvetkov.rushhour.payload.LoginUserRequest;
import com.tsvetkov.rushhour.repository.RoleRepository;
import com.tsvetkov.rushhour.repository.UserRepository;
import com.tsvetkov.rushhour.security.UserPrincipal;
import com.tsvetkov.rushhour.service.UserService;
import com.tsvetkov.rushhour.util.AuthenticationFacade;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

  private final UserRepository userRepository;
  private final ModelMapper mapper;
  private final BCryptPasswordEncoder passwordEncoder;
  private final RoleRepository roleRepository;
  private final AuthenticationFacade authenticationFacade;

  @Autowired
  public UserServiceImpl(
      UserRepository userRepository,
      ModelMapper mapper,
      BCryptPasswordEncoder passwordEncoder,
      RoleRepository roleRepository,
      AuthenticationFacade authenticationFacade) {
    this.userRepository = userRepository;
    this.mapper = mapper;
    this.passwordEncoder = passwordEncoder;
    this.roleRepository = roleRepository;
    this.authenticationFacade = authenticationFacade;
  }

  @Override
  public User persistUser(UserPersistDto userPersistDto) {
    User user = this.mapper.map(userPersistDto, User.class);

    if (this.userRepository.existsByEmail(user.getEmail())) {
      throw new ConflictException("Email Address already in use!");
    }
    user.setPassword(passwordEncoder.encode(user.getPassword()));

    Role userRole =
        this.roleRepository
            .findByName(RoleName.ROLE_USER)
            .orElseThrow(() -> new IllegalStateException("User Role not set."));

    user.getRoles().add(userRole);

    return this.userRepository.save(user);
  }

  @Override
  public User findUserById(int userId) {
    return this.userRepository
        .findById(userId)
        .orElseThrow(() -> new EntityNotFoundException("User doesn't exists by id:" + userId));
  }

  @Override
  public Page<User> findAllUsers(Pageable pageable) {
    return this.userRepository.findAll(pageable);
  }

  @Override
  public User editUser(int id, UserEditDto userDto) {
    UserPrincipal principal = this.authenticationFacade.currentAuthenticatedUser();
    List<String> roles = this.authenticationFacade.currentAuthenticatedUserRoles(principal);

    User userById = findUserById(id);

    if (roles.contains(RoleName.ROLE_ADMIN.toString()) || principal.getId() == id) {
      this.mapper.map(userDto, userById);
      return this.userRepository.save(userById);
    } else {
      throw new AccessDeniedException("Illegal operation. Unauthorized request");
    }
  }

  @Override
  public void editUserPassword(int userId, UserPasswordEditDto userPasswordEditDto) {
    UserPrincipal principal = this.authenticationFacade.currentAuthenticatedUser();
    List<String> roles = this.authenticationFacade.currentAuthenticatedUserRoles(principal);

    User userById = findUserById(userId);
    if (roles.contains(RoleName.ROLE_ADMIN.toString()) || principal.getId() == userId) {

      if (this.passwordEncoder.matches(
          userPasswordEditDto.getOldPassword(), userById.getPassword())) {
        userById.setPassword(this.passwordEncoder.encode(userPasswordEditDto.getNewPassword()));
        this.userRepository.save(userById);
      } else {
        throw new BadCredentialsException("Bad Credentials");
      }

    } else {
      throw new AccessDeniedException("Illegal operation. Unauthorized request");
    }
  }

  @Override
  public void passwordValidation(LoginUserRequest loginUserRequest) {
     User userByEmail = findUserByEmail(loginUserRequest.getEmail());
     if (!this.passwordEncoder.matches(loginUserRequest.getPassword(), userByEmail.getPassword())){
       throw new BadCredentialsException("Bad Credentials");
     }
  }

  @Override
  public void deleteUser(int userId) {
    this.userRepository.deleteById(userId);
  }

  @Override
  public User findUserByEmail(String email) {
    return this.userRepository
        .findByEmail(email)
        .orElseThrow(() -> new EntityNotFoundException("User not found with email: " + email));
  }
}
