package com.tsvetkov.rushhour.service.impl;

import com.tsvetkov.rushhour.domain.entity.Activity;
import com.tsvetkov.rushhour.domain.entity.Appointment;
import com.tsvetkov.rushhour.domain.entity.RoleName;
import com.tsvetkov.rushhour.domain.entity.User;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentActivitiesDto;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentEditDto;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentPersistDto;
import com.tsvetkov.rushhour.exception.customExeption.AccessDeniedException;
import com.tsvetkov.rushhour.exception.customExeption.EntityNotFoundException;
import com.tsvetkov.rushhour.repository.AppointmentRepository;
import com.tsvetkov.rushhour.security.UserPrincipal;
import com.tsvetkov.rushhour.service.ActivityService;
import com.tsvetkov.rushhour.service.AppointmentService;
import com.tsvetkov.rushhour.service.UserService;
import com.tsvetkov.rushhour.util.AuthenticationFacade;
import com.tsvetkov.rushhour.util.validation.DateValidationFactory;
import com.tsvetkov.rushhour.util.validation.DateValidationStrategy;
import com.tsvetkov.rushhour.util.validation.StrategyName;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class AppointmentServiceImpl implements AppointmentService {

  private final AppointmentRepository appointmentRepository;
  private final UserService userService;
  private final ActivityService activityService;
  private final ModelMapper mapper;
  private final AuthenticationFacade authenticationFacade;
  private final DateValidationFactory factory;

  @Autowired
  public AppointmentServiceImpl(
      AppointmentRepository appointmentRepository,
      UserService userService,
      ActivityService activityService,
      ModelMapper mapper,
      AuthenticationFacade authenticationFacade,
      DateValidationFactory factory) {
    this.appointmentRepository = appointmentRepository;
    this.userService = userService;
    this.activityService = activityService;
    this.mapper = mapper;
    this.authenticationFacade = authenticationFacade;
    this.factory = factory;
  }

  @Override
  @Transactional
  public Appointment persistAppointment(AppointmentPersistDto appointmentDto) {
    UserPrincipal principal = this.authenticationFacade.currentAuthenticatedUser();
    List<String> roles = this.authenticationFacade.currentAuthenticatedUserRoles(principal);
    User userById = this.userService.findUserById(principal.getId());
    
    if (!roles.contains(RoleName.ROLE_ADMIN.toString())
        && principal.getId() != userById.getId()) {
      throw new AccessDeniedException("Illegal operation. Unauthorized request");
    }

    Appointment appointment = this.mapper.map(appointmentDto, Appointment.class);
    List<Integer> activities = appointmentDto.getActivities();
    Appointment savedEntity;

    DateValidationStrategy startDateStrategy =
        factory.findStrategy(StrategyName.START_DATE_VALIDATION_STRATEGY);
    startDateStrategy.validateAppointmentDate(
        appointment.getStartDate(), null, this.appointmentRepository.findAll());

    List<Activity> allById = this.activityService.findAllActivitiesById(activities);

    Duration duration = calculateActivitiesDuration(allById);
    LocalDateTime endDate = appointment.getStartDate().plusMinutes(duration.toMinutes());

    DateValidationStrategy endDateStrategy =
        factory.findStrategy(StrategyName.END_DATE_VALIDATION_STRATEGY);
    endDateStrategy.validateAppointmentDate(
        endDate, duration, this.appointmentRepository.findAll());

    appointment.setUser(userById);
    appointment.getActivities().addAll(allById);
    appointment.setEndDate(endDate);

    savedEntity = this.appointmentRepository.save(appointment);

    return savedEntity;
  }

  @Override
  public List<Appointment> findAllAppointments() {
    UserPrincipal principal = this.authenticationFacade.currentAuthenticatedUser();
    List<String> roles = this.authenticationFacade.currentAuthenticatedUserRoles(principal);

    if (!roles.contains(RoleName.ROLE_ADMIN.toString())) {
      return findAppointmentsByUserId(principal.getId());
    }
    return this.appointmentRepository.findAll();
  }

  @Override
  public void deleteAppointment(int appointmentId) {

    UserPrincipal principal = this.authenticationFacade.currentAuthenticatedUser();
    List<String> roles = this.authenticationFacade.currentAuthenticatedUserRoles(principal);

    Appointment appointmentById = findAppointmentById(appointmentId);

    if (roles.contains(RoleName.ROLE_ADMIN.toString())
        || appointmentById.getUser().getId() == principal.getId()) {
      this.appointmentRepository.deleteById(appointmentId);

    } else {
      throw new AccessDeniedException("Illegal operation. Unauthorized request");
    }
  }

  @Override
  public Appointment editAppointment(int appointmentId, AppointmentEditDto appointmentDto) {

    UserPrincipal principal = this.authenticationFacade.currentAuthenticatedUser();
    List<String> roles = this.authenticationFacade.currentAuthenticatedUserRoles(principal);

    Appointment appointmentById = findAppointmentById(appointmentId);

    if (roles.contains(RoleName.ROLE_ADMIN.toString())
        || appointmentById.getUser().getId() == principal.getId()) {

      DateValidationStrategy startDateValidation =
          this.factory.findStrategy(StrategyName.START_DATE_VALIDATION_STRATEGY);

      startDateValidation.validateAppointmentDate(
          appointmentDto.getStartDate(), null, this.appointmentRepository.findAll());

      List<Activity> activities = new ArrayList<>(appointmentById.getActivities());
      Duration duration = calculateActivitiesDuration(activities);

      LocalDateTime endDate = appointmentDto.getStartDate().plusMinutes(duration.toMinutes());

      DateValidationStrategy endDateValidation =
          this.factory.findStrategy(StrategyName.END_DATE_VALIDATION_STRATEGY);
      endDateValidation.validateAppointmentDate(
          endDate, duration, this.appointmentRepository.findAll());

      appointmentById.setStartDate(appointmentDto.getStartDate());
      appointmentById.setEndDate(endDate);

      return this.appointmentRepository.save(appointmentById);

    } else {
      throw new AccessDeniedException("Illegal operation. Unauthorized request");
    }
  }

  @Override
  public Appointment addActivitiesToAppointment(
      int appointmentId, AppointmentActivitiesDto appointmentActivitiesDto) {

    UserPrincipal principal = this.authenticationFacade.currentAuthenticatedUser();
    List<String> roles = this.authenticationFacade.currentAuthenticatedUserRoles(principal);

    Appointment appointmentById = findAppointmentById(appointmentId);
    LocalDateTime startDate = appointmentById.getStartDate();

    if (roles.contains(RoleName.ROLE_ADMIN.toString())
        || appointmentById.getUser().getId() == principal.getId()) {

      List<Activity> allActivitiesById =
          this.activityService.findAllActivitiesById(appointmentActivitiesDto.getActivities());
      appointmentById.getActivities().addAll(allActivitiesById);

      Duration duration =
          calculateActivitiesDuration(new ArrayList<>(appointmentById.getActivities()));
      LocalDateTime endDate = startDate.plusMinutes(duration.toMinutes());

      DateValidationStrategy endDateValidation =
          this.factory.findStrategy(StrategyName.END_DATE_VALIDATION_FOR_NEW_ACTIVITY);

      endDateValidation.validateAppointmentDate(
          endDate, null, this.appointmentRepository.findAll());

      appointmentById.setEndDate(endDate);
      return this.appointmentRepository.save(appointmentById);

    } else {
      throw new AccessDeniedException("Illegal operation. Unauthorized request");
    }
  }

  @Override
  public Appointment findAppointmentById(int appointmentId) {
    return this.appointmentRepository
        .findById(appointmentId)
        .orElseThrow(
            () ->
                new EntityNotFoundException("Appointment doesn't exists by id: " + appointmentId));
  }

  @Override
  public List<Appointment> findAppointmentsByUserId(int userId) {
    return this.appointmentRepository.findAllByUserId(userId);
  }

  private Duration calculateActivitiesDuration(List<Activity> allActivitiesById) {
    return allActivitiesById.stream()
        .map(Activity::getDuration)
        .reduce(Duration.ZERO, Duration::plus);
  }
}
