package com.tsvetkov.rushhour.service.impl;

import com.tsvetkov.rushhour.domain.entity.Activity;
import com.tsvetkov.rushhour.domain.model.binding.ActivityPersistDto;
import com.tsvetkov.rushhour.exception.customExeption.EntityNotFoundException;
import com.tsvetkov.rushhour.exception.customExeption.ConflictException;
import com.tsvetkov.rushhour.repository.ActivityRepository;
import com.tsvetkov.rushhour.service.ActivityService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActivityServiceImpl implements ActivityService {

  private final ActivityRepository activityRepository;
  private final ModelMapper mapper;

  @Autowired
  public ActivityServiceImpl(ActivityRepository activityRepository, ModelMapper mapper) {
    this.activityRepository = activityRepository;
    this.mapper = mapper;
  }

  @Override
  public Activity persistActivity(ActivityPersistDto activityDto) {
    if (this.activityRepository.findByName(activityDto.getName()).isPresent()){
      throw new ConflictException("Activity already exists by name:" + activityDto.getName());
    }
    Activity activity = this.mapper.map(activityDto, Activity.class);
    return this.activityRepository.save(activity);
  }

  @Override
  public List<Activity> findAllActivitiesById(List<Integer> activities) {
    if (activities.isEmpty()) {
      return List.of();
    }
    List<Activity> allActivitiesById = this.activityRepository.findAllById(activities);
    List<Integer> nonExistingActivities = new ArrayList<>();
    for (Integer id : activities) {
      if (!allActivitiesById.stream().map(Activity::getId).collect(Collectors.toList()).contains(id)) {
        nonExistingActivities.add(id);
      }
    }
    if (!nonExistingActivities.isEmpty()) {
      throw new EntityNotFoundException(
              "Activity doesn't exists by id(s): " + nonExistingActivities.toString().replaceAll("\\[|]", ""));
    }
    return allActivitiesById;
  }

  @Override
  public Activity findActivityById(int activityId) {
    return this.activityRepository
        .findById(activityId)
        .orElseThrow(
            () -> new EntityNotFoundException("Activity doesn't exists by id: " + activityId));
  }

  @Override
  public Page<Activity> findAllActivities(Pageable pageable) {
    return this.activityRepository.findAll(pageable);
  }

  @Override
  public Activity editActivity(int activityId, ActivityPersistDto activityDto) {
    Activity activityById = findActivityById(activityId);
    this.mapper.map(activityDto, activityById);
    return this.activityRepository.save(activityById);
  }

  @Override
  public void deleteActivity(int activityId) {
    if (!this.activityRepository.existsById(activityId)){
      throw new EntityNotFoundException("Activity doesn't exists by id: " + activityId);
    }
    this.activityRepository.deleteById(activityId);
  }

}
