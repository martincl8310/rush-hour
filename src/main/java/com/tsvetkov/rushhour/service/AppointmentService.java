package com.tsvetkov.rushhour.service;

import com.tsvetkov.rushhour.domain.entity.Appointment;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentActivitiesDto;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentEditDto;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentPersistDto;

import java.util.List;

public interface AppointmentService {

    Appointment persistAppointment(AppointmentPersistDto appointment);

    List<Appointment> findAllAppointments();

    List<Appointment> findAppointmentsByUserId(int userId);

    void deleteAppointment(int appointmentId);

    Appointment editAppointment(int appointmentId, AppointmentEditDto appointmentDto);

    Appointment findAppointmentById(int appointmentId);

    Appointment addActivitiesToAppointment(int appointmentId, AppointmentActivitiesDto appointmentActivitiesDto);

}
