package com.tsvetkov.rushhour.service;

import com.tsvetkov.rushhour.domain.entity.Activity;
import com.tsvetkov.rushhour.domain.model.binding.ActivityPersistDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ActivityService {
    Activity persistActivity(ActivityPersistDto activity);

    List<Activity> findAllActivitiesById(List<Integer> allActivities);

    Activity findActivityById(int activityId);

    Page<Activity> findAllActivities(Pageable pageable);

    Activity editActivity(int activityId, ActivityPersistDto activityDto);

    void deleteActivity(int activityId);

}
