package com.tsvetkov.rushhour.web.controller;

import com.tsvetkov.rushhour.domain.entity.Appointment;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentActivitiesDto;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentEditDto;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentPersistDto;
import com.tsvetkov.rushhour.domain.model.view.AppointmentViewDto;
import com.tsvetkov.rushhour.service.AppointmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/appointment")
@Api(tags = "Appointment management")
public class AppointmentController {

  private final AppointmentService appointmentService;
  private final ModelMapper modelMapper;

  public AppointmentController(AppointmentService appointmentService, ModelMapper modelMapper) {
    this.appointmentService = appointmentService;
    this.modelMapper = modelMapper;
  }

  @PreAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_USER')")
  @PostMapping("/create")
  @ApiOperation(value = "Create appointment", notes = "Create appointment", nickname = "persistAppointment")
  public ResponseEntity<AppointmentViewDto> persistAppointment(
      @Valid @RequestBody AppointmentPersistDto appointmentDto) {
    Appointment appointment = this.appointmentService.persistAppointment(appointmentDto);
    return ResponseEntity.ok().body(this.modelMapper.map(appointment, AppointmentViewDto.class));
  }

  @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
  @GetMapping("/all")
  @ApiOperation(value = "Find all appointments", notes = "Find all appointments", nickname = "getAllAppointments")
  @ResponseStatus(HttpStatus.OK)
  public List<AppointmentViewDto> getAllAppointments() {
    List<Appointment> allAppointments = this.appointmentService.findAllAppointments();
    return allAppointments.stream()
        .map(user -> this.modelMapper.map(user, AppointmentViewDto.class))
        .collect(Collectors.toList());
  }

  @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
  @DeleteMapping("/delete/{appointmentId}")
  @ApiOperation(value = "Delete appointment", notes = "Delete appointment", nickname = "deleteAppointment")
  public void deleteAppointment(@PathVariable int appointmentId) {
    this.appointmentService.deleteAppointment(appointmentId);
  }

  @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
  @PutMapping("/edit/{appointmentId}")
  @ApiOperation(value = "Edit startDate appointment", notes = "Edit start date appointment", nickname = "editAppointment")
  public ResponseEntity<AppointmentViewDto> editAppointment(
      @PathVariable int appointmentId, @RequestBody AppointmentEditDto appointmentDto) {
    Appointment appointment =
        this.appointmentService.editAppointment(appointmentId, appointmentDto);
    return ResponseEntity.ok().body(this.modelMapper.map(appointment, AppointmentViewDto.class));
  }

  @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
  @PutMapping("/edit/{appointmentId}/activity")
  @ApiOperation(value = "Add new activity to existing appointment", notes = "Add new activity to existing appointment", nickname = "addAppointmentActivities")
  public ResponseEntity<AppointmentViewDto> addAppointmentActivities(
      @PathVariable int appointmentId,
      @RequestBody AppointmentActivitiesDto appointmentActivitiesDto) {
    Appointment appointment =
        this.appointmentService.addActivitiesToAppointment(appointmentId, appointmentActivitiesDto);

    return ResponseEntity.ok().body(this.modelMapper.map(appointment, AppointmentViewDto.class));
  }
}
