package com.tsvetkov.rushhour.web.controller;

import com.tsvetkov.rushhour.domain.entity.Activity;
import com.tsvetkov.rushhour.domain.model.binding.ActivityPersistDto;
import com.tsvetkov.rushhour.domain.model.view.ActivityViewDto;
import com.tsvetkov.rushhour.service.ActivityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/activity")
@Api(tags = "Activity management")
public class ActivityController {

  private final ActivityService activityService;
  private final ModelMapper modelMapper;

  public ActivityController(ActivityService activityService, ModelMapper modelMapper) {
    this.activityService = activityService;
    this.modelMapper = modelMapper;
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @PostMapping("/create")
  @ApiOperation(value = "Create activity", notes = "Create activity", nickname = "persistActivity")
  public ResponseEntity<ActivityViewDto> persistActivity(
      @Valid @RequestBody ActivityPersistDto activityDto) {
    Activity activity = this.activityService.persistActivity(activityDto);
    return ResponseEntity.ok().body(this.modelMapper.map(activity, ActivityViewDto.class));
  }

  @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
  @GetMapping("/all/page")
  @ApiOperation(value = "Find all activities", notes = "Find all  activities", nickname = "findAllActivities")
  public List<ActivityViewDto> findAllActivities(Pageable pageable) {
    Page<Activity> allActivities = this.activityService.findAllActivities(pageable);
    return allActivities.stream()
        .map(activity -> this.modelMapper.map(activity, ActivityViewDto.class))
        .collect(Collectors.toList());
  }
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @PutMapping("/edit/{activityId}")
  @ApiOperation(value = "Edit activity", notes = "Edit activity", nickname = "editActivity")
  public ResponseEntity<ActivityViewDto> editActivity(@PathVariable int activityId,
                                                      @RequestBody ActivityPersistDto activityDto){
      Activity activity = this.activityService.editActivity(activityId, activityDto);
      return ResponseEntity.ok().body(this.modelMapper.map(activity, ActivityViewDto.class));
  }
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/{activityId}")
  @ApiOperation(value = "Find activity by activityId", notes = "Find activity by activityId", nickname = "findActivityById")
  public ResponseEntity<ActivityViewDto> findActivityById(@PathVariable int activityId){

    Activity activityById = this.activityService.findActivityById(activityId);
    ActivityViewDto activityDto = this.modelMapper.map(activityById, ActivityViewDto.class);
    return ResponseEntity.ok().body(activityDto);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @DeleteMapping("/delete/{activityId}")
  @ApiOperation(value = "Delete activity", notes = "Delete activity", nickname = "deleteActivity")
  @ResponseStatus(HttpStatus.OK)
  public void deleteActivity(@PathVariable int activityId){
    this.activityService.deleteActivity(activityId);
  }

}
