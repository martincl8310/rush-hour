package com.tsvetkov.rushhour.web.controller;

import com.tsvetkov.rushhour.domain.entity.User;
import com.tsvetkov.rushhour.domain.model.binding.UserEditDto;
import com.tsvetkov.rushhour.domain.model.binding.UserPasswordEditDto;
import com.tsvetkov.rushhour.domain.model.view.UserViewDto;
import com.tsvetkov.rushhour.payload.ApiResponse;
import com.tsvetkov.rushhour.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
@Api(tags = "User management")
public class UserController {

  private final ModelMapper mapper;
  private final UserService userService;

  public UserController(
      ModelMapper mapper,
      UserService userService) {
    this.mapper = mapper;
    this.userService = userService;
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/{userId}")
  @ApiOperation(value = "Find user by userId", notes = "Find user by userId", nickname = "findUserById")
  public ResponseEntity<UserViewDto> findUserById(@PathVariable int userId) {

    User userById = this.userService.findUserById(userId);
    UserViewDto userDto = this.mapper.map(userById, UserViewDto.class);
    return ResponseEntity.ok().body(userDto);
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @GetMapping("/all/page")
  @ApiOperation(value = "Find all users", notes = "Find all users", nickname = "findAllUsers")
  @ResponseStatus(HttpStatus.OK)
  public List<UserViewDto> findAllUsers(Pageable pageable) {
    Page<User> allUsers = this.userService.findAllUsers(pageable);
    return allUsers.stream()
        .map(user -> this.mapper.map(user, UserViewDto.class))
        .collect(Collectors.toList());
  }

  @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
  @PutMapping("/edit/{userId}")
  @ApiOperation(value = "Edit user", notes = "Edit user", nickname = "editUser")
  public ResponseEntity<UserViewDto> editUser(
      @PathVariable int userId, @RequestBody UserEditDto userDto) {
    User user = this.userService.editUser(userId, userDto);

    return ResponseEntity.ok().body(this.mapper.map(user, UserViewDto.class));
  }

  @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
  @PutMapping("/edit/password/{userId}")
  @ApiOperation(value = "Edit user's password", notes = "Edit user's password", nickname = "editUserPassword")
  public ResponseEntity<?> editUserPassword(
      @PathVariable int userId, @RequestBody UserPasswordEditDto userPasswordEditDto) {
    if (!userPasswordEditDto.getNewPassword().equals(userPasswordEditDto.getConfirmPassword())) {
      throw new BadCredentialsException("Bad Credentials");
    }
    this.userService.editUserPassword(userId, userPasswordEditDto);
    return ResponseEntity.ok().body(new ApiResponse(true, "Password  edited successfully"));
  }

  @PreAuthorize("hasRole('ROLE_ADMIN')")
  @DeleteMapping("/delete/{userId}")
  @ApiOperation(value = "Delete user", notes = "Delete user", nickname = "deleteUser")
  public ResponseEntity<?> deleteUser(@PathVariable int userId) {
    this.userService.deleteUser(userId);
    return ResponseEntity.ok()
        .body(new ApiResponse(true, "User with id:" + userId + " deleted successfully"));
  }
}
