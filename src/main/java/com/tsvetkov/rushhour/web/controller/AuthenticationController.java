package com.tsvetkov.rushhour.web.controller;

import com.tsvetkov.rushhour.domain.model.binding.UserPersistDto;
import com.tsvetkov.rushhour.payload.ApiResponse;
import com.tsvetkov.rushhour.payload.JwtAuthenticationResponse;
import com.tsvetkov.rushhour.payload.LoginUserRequest;
import com.tsvetkov.rushhour.security.JwtTokenProvider;
import com.tsvetkov.rushhour.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
@Api(tags = "Authentication management")
public class AuthenticationController {

  private final UserService userService;
  private final AuthenticationManager authenticationManager;
  private final JwtTokenProvider tokenProvider;

  @Autowired
  public AuthenticationController(
          UserService userService,
          AuthenticationManager authenticationManager,
          JwtTokenProvider tokenProvider) {
    this.userService = userService;
    this.authenticationManager = authenticationManager;
    this.tokenProvider = tokenProvider;
  }

  @PostMapping("/login")
  @ApiOperation(value = "Login user", notes = "Login user", nickname = "authenticateUser")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginUserRequest loginUserRequest) {
    this.userService.passwordValidation(loginUserRequest);
    Authentication authentication =
        this.authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(
                loginUserRequest.getEmail(), loginUserRequest.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = this.tokenProvider.generateToken(authentication);
    return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
  }

  @PostMapping("/signup")
  @ApiOperation(value = "Sign up user", notes = "Sign up user", nickname = "persistUser")
  public ResponseEntity<?> persistUser(@Valid @RequestBody UserPersistDto userDto) {
    this.userService.persistUser(userDto);
    return ResponseEntity.ok().body(new ApiResponse(true, "User registered successfully"));
  }
}
