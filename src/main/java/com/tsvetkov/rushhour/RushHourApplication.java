package com.tsvetkov.rushhour;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

@SpringBootApplication
public class RushHourApplication {

    public static void main(String[] args) {
        SpringApplication.run(RushHourApplication.class, args);
    }

}
