package com.tsvetkov.rushhour.util.validation.appointmentValidation;

import com.tsvetkov.rushhour.domain.entity.Appointment;
import com.tsvetkov.rushhour.exception.customExeption.ConflictException;
import com.tsvetkov.rushhour.util.validation.DateValidationStrategy;
import com.tsvetkov.rushhour.util.validation.StrategyName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class StartDateAppointmentValidation implements DateValidationStrategy {

  private final AppointmentValidation appointmentValidation;

  @Autowired
  public StartDateAppointmentValidation(AppointmentValidation appointmentValidation) {
    this.appointmentValidation = appointmentValidation;
  }

  @Override
  public void validateAppointmentDate(
      LocalDateTime startDate, Duration duration, List<Appointment> appointments) {
    if (!appointmentValidation.isStartDateAvailable(startDate, appointments)) {
      throw new ConflictException("This appointment is already taken.");
    }
  }

  @Override
  public StrategyName getStrategyName() {
    return StrategyName.START_DATE_VALIDATION_STRATEGY;
  }
}
