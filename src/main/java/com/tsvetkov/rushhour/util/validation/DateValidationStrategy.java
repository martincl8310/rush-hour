package com.tsvetkov.rushhour.util.validation;

import com.tsvetkov.rushhour.domain.entity.Appointment;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

public interface DateValidationStrategy {

    void validateAppointmentDate(LocalDateTime dateTime, Duration duration, List<Appointment> appointments);

    StrategyName getStrategyName();
}
