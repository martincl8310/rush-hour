package com.tsvetkov.rushhour.util.validation.appointmentValidation;

import com.tsvetkov.rushhour.domain.entity.Appointment;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class AppointmentValidation {

  public boolean isStartDateAvailable(LocalDateTime startDate, List<Appointment> appointments) {
    boolean isAvailable = true;
    for (Appointment appointment : appointments) {
      if (appointment.getStartDate().equals(startDate)
          || appointment.getEndDate().equals(startDate)
          || (startDate.isAfter(appointment.getStartDate())
              && startDate.isBefore(appointment.getEndDate()))) {
        isAvailable = false;
        break;
      }
    }
    return isAvailable;
  }

  public boolean isEndDateAvailable(
      LocalDateTime endDate, Duration duration, List<Appointment> appointments) {
    boolean isAvailable = true;
    for (Appointment appointment : appointments) {
      if (appointment.getEndDate().equals(endDate)
          || appointment.getStartDate().equals(endDate)
          || (endDate.isAfter(appointment.getStartDate())
              && endDate.minus(duration).isBefore(appointment.getEndDate()))) {
        isAvailable = false;
      }
    }
    return isAvailable;
  }

  public boolean isEndDateAvailableForAddNewActivity(
      LocalDateTime endDate, List<Appointment> appointments) {
    boolean isAvailable = true;
    for (Appointment appointment : appointments) {
      if (appointment.getEndDate().equals(endDate)
          || appointment.getStartDate().equals(endDate)
          || (endDate.isAfter(appointment.getStartDate())
              && endDate.isBefore(appointment.getEndDate()))) {
        isAvailable = false;
      }
    }
    return isAvailable;
  }
}
