package com.tsvetkov.rushhour.util.validation.appointmentValidation;

import com.tsvetkov.rushhour.domain.entity.Appointment;
import com.tsvetkov.rushhour.exception.customExeption.ConflictException;
import com.tsvetkov.rushhour.util.validation.DateValidationStrategy;
import com.tsvetkov.rushhour.util.validation.StrategyName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

@Component
public class EndDateValidationForAddActivity implements DateValidationStrategy {

  private final AppointmentValidation appointmentValidation;

  @Autowired
  public EndDateValidationForAddActivity(AppointmentValidation appointmentValidation) {
    this.appointmentValidation = appointmentValidation;
  }

  @Override
  public void validateAppointmentDate(
      LocalDateTime endDate, Duration duration, List<Appointment> appointments) {
    if (!this.appointmentValidation.isEndDateAvailableForAddNewActivity(endDate, appointments)) {
      throw new ConflictException(
          "The duration of appointment's activities is"
              + " overlapping with already existing appointment.");
    }
  }

  @Override
  public StrategyName getStrategyName() {
    return StrategyName.END_DATE_VALIDATION_FOR_NEW_ACTIVITY;
  }
}
