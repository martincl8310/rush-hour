package com.tsvetkov.rushhour.util.validation;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Component
public class DateValidationFactory {

  private Map<StrategyName, DateValidationStrategy> strategies;

  public DateValidationFactory(Set<DateValidationStrategy> strategySet) {
    createStrategy(strategySet);
  }

  public DateValidationStrategy findStrategy(StrategyName strategyName) {
    return strategies.get(strategyName);
  }

  private void createStrategy(Set<DateValidationStrategy> strategySet) {
    strategies = new HashMap<>();
    strategySet.forEach(strategy -> strategies.put(strategy.getStrategyName(), strategy));
  }
}
