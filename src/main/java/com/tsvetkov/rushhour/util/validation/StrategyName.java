package com.tsvetkov.rushhour.util.validation;

public enum StrategyName {
    START_DATE_VALIDATION_STRATEGY, END_DATE_VALIDATION_STRATEGY, END_DATE_VALIDATION_FOR_NEW_ACTIVITY
}
