package com.tsvetkov.rushhour.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.Duration;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "activity")
public class Activity extends BaseEntity {
  @Column(name = "name", nullable = false, unique = true)
  private String name;

  @Column(name = "duration", nullable = false)
  private Duration duration;

  @Column(name = "price", nullable = false)
  private BigDecimal price;

  @ManyToMany(mappedBy = "activities")
  private Set<Appointment> appointments = new HashSet<>();

  public Activity() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Duration getDuration() {
    return duration;
  }

  public void setDuration(Duration duration) {
    this.duration = duration;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }

  public Set<Appointment> getAppointments() {
    return appointments;
  }

  public void setAppointments(Set<Appointment> appointments) {
    this.appointments = appointments;
  }

}

