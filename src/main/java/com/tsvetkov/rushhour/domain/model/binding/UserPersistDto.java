package com.tsvetkov.rushhour.domain.model.binding;

import javax.validation.constraints.*;

public class UserPersistDto {
  @NotBlank(message = "First name is required")
  @Size(min = 2,max = 30, message = "First name should be between 2 or 30 characters")
  private String firstName;

  @Size(min = 2, max = 30, message = "Last name should be between 2 or 30 characters")
  @NotBlank(message = "Last name is required")
  private String lastName;

  @Email(message = "Please provide a valid email address")
  private String email;

  @NotBlank(message = "Password is required")
  @Size(min = 3, max = 30, message = "Password should be between 3 or 30 characters")
  private String password;

  public UserPersistDto() {}

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}

