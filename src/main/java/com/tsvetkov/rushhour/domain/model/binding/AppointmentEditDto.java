package com.tsvetkov.rushhour.domain.model.binding;

import java.time.LocalDateTime;

public class AppointmentEditDto {

    private LocalDateTime startDate;

    public AppointmentEditDto() {
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

}
