package com.tsvetkov.rushhour.domain.model.view;

import java.util.Set;

public class AppointmentViewDto{
  private int id;
  private String startDate;
  private String endDate;
  private Set<ActivityViewDto> activities;

  public AppointmentViewDto() {}

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }

  public Set<ActivityViewDto> getActivities() {
    return activities;
  }

  public void setActivities(Set<ActivityViewDto> activities) {
    this.activities = activities;
  }
}

