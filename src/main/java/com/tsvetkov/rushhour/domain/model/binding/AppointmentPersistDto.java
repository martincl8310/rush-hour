package com.tsvetkov.rushhour.domain.model.binding;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

public class AppointmentPersistDto {

  private LocalDateTime startDate;

  @NotEmpty(message = "Required activities")
  @NotNull
  private List<Integer> activities;

  public AppointmentPersistDto() {}

  public LocalDateTime getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDateTime startDate) {
    this.startDate = startDate;
  }

  public List<Integer> getActivities() {
    return activities;
  }

  public void setActivities(List<Integer> activities) {
    this.activities = activities;
  }

}

