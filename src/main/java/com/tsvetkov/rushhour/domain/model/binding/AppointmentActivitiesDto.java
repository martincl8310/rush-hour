package com.tsvetkov.rushhour.domain.model.binding;

import java.util.List;

public class AppointmentActivitiesDto {
    private List<Integer> activities;

    public List<Integer> getActivities() {
        return activities;
    }

    public void setActivities(List<Integer> activities) {
        this.activities = activities;
    }
}
