package com.tsvetkov.rushhour.domain.model.binding;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserPasswordEditDto {

    @NotBlank(message = "Old password is required")
    @Size(min = 3, max = 30, message = "Password should be between 3 or 30 characters")
    private String oldPassword;

    @NotBlank(message = "New password is required")
    @Size(min = 3, max = 30, message = "Password should be between 3 or 30 characters")
    private String newPassword;

    @NotBlank(message = "Password is required")
    @Size(min = 3, max = 30, message = "Password should be between 3 or 30 characters")
    private String confirmPassword;

    public UserPasswordEditDto() {
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
