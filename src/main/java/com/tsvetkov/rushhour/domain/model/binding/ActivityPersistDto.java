package com.tsvetkov.rushhour.domain.model.binding;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import java.time.Duration;

public class ActivityPersistDto {

  @NotBlank(message = "Activity name is required")
  private String name;

  private Duration duration;

  @DecimalMin(value = "5.0")
  private double price;

  public ActivityPersistDto() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public Duration getDuration() {
    return duration;
  }

  public void setDuration(Duration duration) {
    this.duration = duration;
  }
}

