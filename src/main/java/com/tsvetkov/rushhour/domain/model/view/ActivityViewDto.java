package com.tsvetkov.rushhour.domain.model.view;

import java.time.Duration;

public class ActivityViewDto{
  private int id;
  private String name;
  private Duration duration;
  private double price;

  public ActivityViewDto() {}

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public Duration getDuration() {
    return duration;
  }

  public void setDuration(Duration duration) {
    this.duration = duration;
  }
}

