package com.tsvetkov.rushhour.domain.model.view;

import java.util.Set;

public class UserViewDto{
  private int id;
  private String firstName;
  private String lastName;
  private Set<AppointmentViewDto> appointments;

  public UserViewDto() {}

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public Set<AppointmentViewDto> getAppointments() {
    return appointments;
  }

  public void setAppointments(Set<AppointmentViewDto> appointments) {
    this.appointments = appointments;
  }
}

