package com.tsvetkov.rushhour.exception;

import com.tsvetkov.rushhour.exception.customExeption.*;
import com.tsvetkov.rushhour.exception.validation.ValidationError;
import com.tsvetkov.rushhour.exception.validation.ValidationErrorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

  private final ValidationErrorFactory validationErrorFactory;

  @Autowired
  public RestExceptionHandler(ValidationErrorFactory validationErrorFactory) {
    this.validationErrorFactory = validationErrorFactory;
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
                                                                HttpHeaders headers,
                                                                HttpStatus status,
                                                                WebRequest request) {
    ValidationError validationError = validationErrorFactory.fromBindingErrors(exception.getBindingResult());
    ApiError apiError = new ApiError(HttpStatus.BAD_REQUEST, exception);
    apiError.getValidationErrors().add(validationError);
    return buildResponseEntity(apiError);
  }

  @ExceptionHandler(BadCredentialsException.class)
  public ResponseEntity<Object> handleIncorrectPasswordException(Exception exception){
    ApiError apiError = new ApiError(HttpStatus.UNAUTHORIZED);
    apiError.setMessage(exception.getMessage());
    return buildResponseEntity(apiError);
  }


  @ExceptionHandler(AccessDeniedException.class)
  public ResponseEntity<Object> handleNOtAuthorizeException(Exception exception){
    ApiError apiError = new ApiError(HttpStatus.FORBIDDEN);
    apiError.setMessage(exception.getMessage());
    return buildResponseEntity(apiError);
  }

  @ExceptionHandler(ConflictException.class)
  public ResponseEntity<Object> handleStartDateExistsException(Exception exception) {
    ApiError apiError = new ApiError(HttpStatus.CONFLICT);
    apiError.setMessage(exception.getMessage());
    return buildResponseEntity(apiError);
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<Object> handleEntityNotFound(Exception exception) {
    ApiError apiError = new ApiError(HttpStatus.NOT_FOUND);
    apiError.setMessage(exception.getMessage());
    return buildResponseEntity(apiError);
  }

  @Override
  protected ResponseEntity<Object> handleHttpMessageNotReadable(
      HttpMessageNotReadableException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    String error = "Malformed JSON request";
    return buildResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, error, ex));
  }

  private ResponseEntity<Object> buildResponseEntity(ApiError apiError) {
    return new ResponseEntity<>(apiError, apiError.getStatus());
  }
}
