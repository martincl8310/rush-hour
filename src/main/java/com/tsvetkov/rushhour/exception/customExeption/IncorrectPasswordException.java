package com.tsvetkov.rushhour.exception.customExeption;

public class IncorrectPasswordException extends RuntimeException {

  public IncorrectPasswordException(String message) {
    super(message);
  }
}
