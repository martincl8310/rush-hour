package com.tsvetkov.rushhour.exception.customExeption;

public class AccessDeniedException extends RuntimeException {

    public AccessDeniedException(String message) {
        super(message);
    }
}
