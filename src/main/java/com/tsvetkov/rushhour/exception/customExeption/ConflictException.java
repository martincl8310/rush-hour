package com.tsvetkov.rushhour.exception.customExeption;

public class ConflictException extends RuntimeException {

    public ConflictException(String message) {
        super(message);
    }
}
