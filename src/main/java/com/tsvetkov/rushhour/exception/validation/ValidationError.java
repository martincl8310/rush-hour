package com.tsvetkov.rushhour.exception.validation;

import java.util.HashMap;
import java.util.Map;

public class ValidationError {
    private final String errorMessage;
    private final Map<String, String> errors = new HashMap<>();

    public ValidationError(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
