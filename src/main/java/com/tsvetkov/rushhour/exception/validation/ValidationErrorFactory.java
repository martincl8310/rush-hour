package com.tsvetkov.rushhour.exception.validation;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

public final class ValidationErrorFactory {


  public  ValidationError fromBindingErrors(BindingResult result) {
    ValidationError validationError =
        new ValidationError("Validation failed. " + result.getErrorCount() + " error(s)");
    for (FieldError error : result.getFieldErrors()) {
      validationError.getErrors().put(error.getField(), error.getDefaultMessage());
    }
    return validationError;
  }
}
