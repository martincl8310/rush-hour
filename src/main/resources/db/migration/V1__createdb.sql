CREATE TABLE IF NOT EXISTS `user`
(
    id         INT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(50)         NOT NULL,
    last_name  VARCHAR(50)         NOT NULL,
    email      varchar(100) UNIQUE NOT NULL,
    password   varchar(100)        NOT NULL
);

CREATE TABLE IF NOT EXISTS role
(
    id   INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(100)

);

CREATE TABLE IF NOT EXISTS user_role
(
    user_id INT NOT NULL,
    role_id INT NOT NULL,
    CONSTRAINT fk_user_role FOREIGN KEY (user_id) REFERENCES `user` (id),
    CONSTRAINT fk_role_user FOREIGN KEY (role_id) REFERENCES role (id)
);

CREATE TABLE IF NOT exists appointment
(
    id         INT PRIMARY KEY AUTO_INCREMENT,
    start_date DATETIME NOT NULL,
    end_date   DATETIME NOT NULL,
    user_id    INT,
    CONSTRAINT fk_appointment_user FOREIGN KEY (user_id) REFERENCES `user` (id)
);
CREATE TABLE IF NOT EXISTS activity
(
    id       INT PRIMARY KEY AUTO_INCREMENT,
    duration BIGINT UNSIGNED NOT NULL,
    name     VARCHAR(100)    NOT NULL UNIQUE,
    price    DECIMAL         NOT NULL
);

CREATE TABLE IF NOT EXISTS appointment_activity
(
    appointment_id INT,
    activity_id    INT,
    CONSTRAINT fk_appointment_activity FOREIGN KEY (appointment_id) REFERENCES appointment (id),
    CONSTRAINT fk_activity_appointment FOREIGN KEY (activity_id) REFERENCES activity (id) ON DELETE CASCADE
);

INSERT INTO role(name)
VALUES ('ROLE_USER');
INSERT INTO role(name)
VALUES ('ROLE_ADMIN');

INSERT INTO `user`(first_name, last_name, email, password)
VALUES ('Mitko', 'Dimitrov', 'mitko@abv.bg', '$2y$12$AEuMdqwAXUGFGJSvCmFn1eOc2uqgNMX8axkwJqknmGkq4NF4uoifq');

INSERT INTO user_role(user_id, role_id)
VALUES (1, 2);
