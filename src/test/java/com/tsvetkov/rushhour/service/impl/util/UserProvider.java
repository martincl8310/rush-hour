package com.tsvetkov.rushhour.service.impl.util;

import com.tsvetkov.rushhour.domain.entity.Role;
import com.tsvetkov.rushhour.domain.entity.User;
import com.tsvetkov.rushhour.domain.model.binding.UserEditDto;
import com.tsvetkov.rushhour.domain.model.binding.UserPasswordEditDto;
import com.tsvetkov.rushhour.domain.model.binding.UserPersistDto;
import com.tsvetkov.rushhour.security.UserPrincipal;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.stream.Collectors;

public class UserProvider {

    public static UserPersistDto userPersistDto(){
        UserPersistDto userPersistDto = new UserPersistDto();
        userPersistDto.setEmail("vivi@abv.bg");
        return userPersistDto;
    }

    public static User userFromDb(){
        Role role = RoleProvider.userRole();
        User user = new User();
        user.setFirstName("Mitko");
        user.setLastName("Grigorov");
        user.setEmail("mitko@abv.bg");
        user.setPassword("12");
        user.getRoles().add(role);
        user.setId(1);

        return user;
    }

    public static UserPasswordEditDto userPasswordEditDto(){
        UserPasswordEditDto userDto = new UserPasswordEditDto();
        userDto.setNewPassword("1");
        userDto.setConfirmPassword("1");

        return userDto;
    }
    public static UserEditDto userEditDto(){
        UserEditDto userEditDto = new UserEditDto();
        userEditDto.setFirstName("minka");
        userEditDto.setLastName("minkova");

        return userEditDto;
    }

    public static UserPrincipal userPrincipal(){
         User user = UserProvider.userFromDb();
        List<GrantedAuthority> authorities = user.getRoles()
                .stream().map(role -> new SimpleGrantedAuthority(role.getName().name()))
                .collect(Collectors.toList());

        UserPrincipal userPrincipal = new UserPrincipal(user.getId(),
                user.getFirstName(), user.getLastName(),user.getEmail(),user.getPassword(), authorities);

        return userPrincipal;
    }
}

