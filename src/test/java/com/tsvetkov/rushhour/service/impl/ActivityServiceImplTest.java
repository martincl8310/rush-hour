package com.tsvetkov.rushhour.service.impl;

import com.tsvetkov.rushhour.domain.entity.Activity;
import com.tsvetkov.rushhour.domain.model.binding.ActivityPersistDto;
import com.tsvetkov.rushhour.exception.customExeption.ConflictException;
import com.tsvetkov.rushhour.exception.customExeption.EntityNotFoundException;
import com.tsvetkov.rushhour.repository.ActivityRepository;
import com.tsvetkov.rushhour.service.impl.util.ActivityProvider;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ActivityServiceImplTest {

    @InjectMocks
    private ActivityServiceImpl activityService;

    @Mock
    private ActivityRepository activityRepository;

    @Mock
    private ModelMapper modelMapper;

    @DisplayName("Persist activity successfully")
    @Test
    public void testPersistActivity() {
        ActivityPersistDto activityPersistDto = ActivityProvider.activityPersistDto();
        Activity activity = ActivityProvider.activityFromDb();

        when(this.modelMapper.map(activityPersistDto, Activity.class)).thenReturn(activity);
        when(this.activityRepository.save(any(Activity.class))).thenReturn(activity);

        Activity resultAct = this.activityService.persistActivity(activityPersistDto);

        assertEquals(activity, resultAct);
    }
    @DisplayName("Fail to persist activity if activityName already exists")
    @Test
    public void testFailToPersistActivity(){
        ActivityPersistDto activityPersistDto = ActivityProvider.activityPersistDto();
        Activity activity = ActivityProvider.activityFromDb();

        when(this.activityRepository.findByName(activityPersistDto.getName())).thenReturn(Optional.of(activity));

        assertThrows(ConflictException.class, () -> this.activityService.persistActivity(activityPersistDto));
    }
    @DisplayName("Find successfully activity by activityId")
    @Test
    public void testFindActivityById() {
        ActivityPersistDto activityPersistDto = ActivityProvider.activityPersistDto();
        Activity activityToPersist = ActivityProvider.activityFromDb();

        activityToPersist.setDuration(activityPersistDto.getDuration());
        activityToPersist.setName(activityPersistDto.getName());
        activityToPersist.setPrice(BigDecimal.valueOf(activityPersistDto.getPrice()));
        activityToPersist.setId(1);

        when(this.activityRepository.findById(anyInt())).thenReturn(Optional.of(activityToPersist));

        Activity activityById = this.activityService.findActivityById(1);

        assertEquals(activityToPersist, activityById);
    }
    @DisplayName("Activity cannot be found by id if doesn't exists")
    @Test
    public void testNoActivityFoundById() {
        ActivityPersistDto activityPersistDto = ActivityProvider.activityPersistDto();
        Activity activityToPersist = ActivityProvider.activityFromDb();

        activityToPersist.setDuration(activityPersistDto.getDuration());
        activityToPersist.setName(activityPersistDto.getName());
        activityToPersist.setPrice(BigDecimal.valueOf(activityPersistDto.getPrice()));

        when(this.activityRepository.findById(anyInt())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> activityService.findActivityById(1));
    }
    @DisplayName("Find all activities by id if they exists")
    @Test
    public void testFindAllActivitiesById() {
        List<Integer> ids = List.of(1, 2);
        ActivityPersistDto activityPersistDto = ActivityProvider.activityPersistDto();

        Activity firstActivity = ActivityProvider.activityFromDb();
        firstActivity.setDuration(activityPersistDto.getDuration());
        firstActivity.setName(activityPersistDto.getName());
        firstActivity.setPrice(BigDecimal.valueOf(activityPersistDto.getPrice()));

        Activity secondActivity = ActivityProvider.activityFromDb();
        secondActivity.setDuration(activityPersistDto.getDuration());
        secondActivity.setName(activityPersistDto.getName());
        secondActivity.setPrice(BigDecimal.valueOf(activityPersistDto.getPrice()));
        secondActivity.setId(2);

        List<Activity> activities = List.of(firstActivity, secondActivity);

        when(this.activityRepository.findAllById(anyList())).thenReturn(activities);

        List<Activity> result = this.activityService.findAllActivitiesById(ids);

        assertEquals(activities, result);
    }
    @DisplayName("Activities cannot be found by id and throw EntityNotFoundException")
    @Test
    public void testNoActivitiesFoundById() {
        List<Integer> ids = List.of(3, 4);
        ActivityPersistDto activityPersistDto = ActivityProvider.activityPersistDto();

        Activity activity = ActivityProvider.activityFromDb();
        activity.setDuration(activityPersistDto.getDuration());
        activity.setName(activityPersistDto.getName());
        activity.setPrice(BigDecimal.valueOf(activityPersistDto.getPrice()));
        List<Activity> activities = List.of(activity);

        when(this.activityRepository.findAllById(anyList())).thenReturn(activities);

        assertThrows(EntityNotFoundException.class, () -> activityService.findAllActivitiesById(ids));
    }
    @DisplayName("Edit activity successfully by activityId")
    @Test
    public void testEditActivity() {
        Activity activityFromDb = ActivityProvider.activityFromDb();
        ActivityPersistDto activityPersistDto = ActivityProvider.activityPersistDto();
        activityFromDb.setDuration(activityPersistDto.getDuration());
        activityFromDb.setName(activityPersistDto.getName());
        activityFromDb.setPrice(BigDecimal.valueOf(activityPersistDto.getPrice()));

        when(this.activityRepository.findById(1)).thenReturn(Optional.of(activityFromDb));
        when(this.activityRepository.save(any(Activity.class))).thenReturn(activityFromDb);

        Activity result = activityService.editActivity(1, activityPersistDto);

        assertEquals(activityFromDb, result);
    }
    @DisplayName("Delete activity successfully by activityId")
    @Test
    public void testDeleteActivity(){
         Activity activity = ActivityProvider.activityFromDb();

         when(this.activityRepository.existsById(activity.getId())).thenReturn(true);

         this.activityService.deleteActivity(activity.getId());

         verify(this.activityRepository, times(1)).deleteById(activity.getId());

    }
    @DisplayName("Fail to delete activity if doesn't exists")
    @Test
    public void testFailToDeleteActivity(){
        Activity activity = ActivityProvider.activityFromDb();

        when(this.activityRepository.existsById(activity.getId())).thenReturn(false);

        assertThrows(EntityNotFoundException.class, () ->activityService.deleteActivity(activity.getId()));
    }
    @DisplayName("Get all activities from database successfully")
    @Test
    public void testFindAllActivities(){
        Page<Activity> activities = Mockito.mock(Page.class);

        when(this.activityRepository.findAll(Mockito.isA(Pageable.class))).thenReturn(activities);

        Page<Activity> allActivities = this.activityService.findAllActivities(Mockito.mock(Pageable.class));

        assertEquals(activities, allActivities);

        verify(this.activityRepository, times(1)).findAll(Mockito.isA(Pageable.class));
    }
}

