package com.tsvetkov.rushhour.service.impl;

import com.tsvetkov.rushhour.domain.entity.Role;
import com.tsvetkov.rushhour.domain.entity.RoleName;
import com.tsvetkov.rushhour.domain.entity.User;
import com.tsvetkov.rushhour.domain.model.binding.UserEditDto;
import com.tsvetkov.rushhour.domain.model.binding.UserPasswordEditDto;
import com.tsvetkov.rushhour.domain.model.binding.UserPersistDto;
import com.tsvetkov.rushhour.exception.customExeption.AccessDeniedException;
import com.tsvetkov.rushhour.exception.customExeption.ConflictException;
import com.tsvetkov.rushhour.exception.customExeption.EntityNotFoundException;
import com.tsvetkov.rushhour.repository.RoleRepository;
import com.tsvetkov.rushhour.repository.UserRepository;
import com.tsvetkov.rushhour.security.UserPrincipal;
import com.tsvetkov.rushhour.service.impl.util.RoleProvider;
import com.tsvetkov.rushhour.service.impl.util.UserProvider;
import com.tsvetkov.rushhour.util.AuthenticationFacade;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private BCryptPasswordEncoder encoder;

    @Mock
    private ModelMapper mapper;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private AuthenticationFacade facade;

    @DisplayName("Persist a user successfully to the database")
    @Test
    public void testPersistUser() {
        Role role = RoleProvider.userRole();
        UserPersistDto userPersistDto = UserProvider.userPersistDto();

        User user = new User();
        user.setRoles(new HashSet<>());
        user.getRoles().add(role);
        user.setId(1);

        when(this.mapper.map(userPersistDto, User.class)).thenReturn(user);
        when(this.userRepository.save(any(User.class))).thenReturn(user);
        when(this.roleRepository.findByName(any(RoleName.class))).thenReturn(Optional.of(role));

        User result = this.userService.persistUser(userPersistDto);

        assertEquals(user, result);

        verify(this.userRepository, times(1)).save(user);
    }

    @DisplayName("Cannot persist a user to the database if email already exists")
    @Test
    public void testIfUserExistByEmail() {
        UserPersistDto userPersistDto = UserProvider.userPersistDto();
        User user = new User();
        user.setEmail(userPersistDto.getEmail());

        when(this.mapper.map(userPersistDto, User.class)).thenReturn(user);
        when(this.userRepository.existsByEmail(user.getEmail())).thenReturn(true);

        assertThrows(
                ConflictException.class, () -> this.userService.persistUser(userPersistDto));

        verify(this.userRepository, never()).save(user);
    }
    @DisplayName("Cannot persist a user to the database if role is not set")
    @Test
    public void testIfUserRoleIsSet() {
        UserPersistDto userPersistDto = UserProvider.userPersistDto();

        User user = new User();
        user.setEmail(userPersistDto.getEmail());

        when(this.mapper.map(userPersistDto, User.class)).thenReturn(user);

        assertThrows(
                IllegalStateException.class, () -> this.userService.persistUser(userPersistDto));

        verify(this.userRepository, never()).save(user);
    }
    @DisplayName("Edit firstName and lastName of existing user successfully")
    @Test
    public void testEditUsersFirstNameAndLastName() {
        User user = UserProvider.userFromDb();
        UserEditDto userEditDto = UserProvider.userEditDto();

        user.setFirstName(userEditDto.getFirstName());
        user.setLastName(userEditDto.getLastName());

        when(this.facade.currentAuthenticatedUser()).thenReturn(UserPrincipal.create(user));
        when(this.userRepository.findById(anyInt())).thenReturn(Optional.of(user));
        when(this.userRepository.save(any(User.class))).thenReturn(user);

        User result = this.userService.editUser(1, userEditDto);

        assertEquals(user, result);
    }
    @DisplayName("Throw AccessDeniedException if authenticated user's id is not correct")
    @Test
    public void testUnauthorizedUserRequest() {
        User user = UserProvider.userFromDb();

        User authenticatedUser = new User();
        authenticatedUser.setId(2);

        UserEditDto userEditDto = UserProvider.userEditDto();

        when(this.facade.currentAuthenticatedUser())
                .thenReturn(UserPrincipal.create(authenticatedUser));
        when(this.userRepository.findById(anyInt())).thenReturn(Optional.of(user));

        assertThrows(
                AccessDeniedException.class,
                () -> this.userService.editUser(1, userEditDto));
    }
    @DisplayName("Edit existing user's password successfully")
    @Test
    public void testEditUserPassword() {
        User user = UserProvider.userFromDb();

        User authenticatedUser = new User();
        authenticatedUser.setId(1);

        UserPasswordEditDto userDto = UserProvider.userPasswordEditDto();
        userDto.setOldPassword(user.getPassword());

        User editUser = new User();
        editUser.setPassword(userDto.getNewPassword());
        editUser.setId(user.getId());

        when(this.facade.currentAuthenticatedUser())
                .thenReturn(UserPrincipal.create(authenticatedUser));
        when(this.encoder.matches(anyString(), anyString())).thenReturn(true);
        when(this.userRepository.findById(anyInt())).thenReturn(Optional.of(user));

        this.userService.editUserPassword(1, userDto);

        assertEquals(editUser.getPassword(), userDto.getNewPassword());
    }
    @DisplayName("User fails to change an already existing password")
    @Test
    public void testIncorrectPasswordFromUser() {
        User user = UserProvider.userFromDb();

        User authenticatedUser = new User();
        authenticatedUser.setId(1);

        UserPasswordEditDto userDto = UserProvider.userPasswordEditDto();
        userDto.setOldPassword(user.getPassword());

        user.setPassword(userDto.getNewPassword());
        user.setId(user.getId());

        when(this.facade.currentAuthenticatedUser())
                .thenReturn(UserPrincipal.create(authenticatedUser));
        when(this.encoder.matches(anyString(), anyString())).thenReturn(false);
        when(this.userRepository.findById(anyInt())).thenReturn(Optional.of(user));

        assertThrows(BadCredentialsException.class, () -> this.userService.editUserPassword(1, userDto));

        verify(this.userRepository, never()).save(user);
    }
    @DisplayName("Delete a user from the database successfully")
    @Test
    public void testDeleteUser(){
         User user = UserProvider.userFromDb();

         this.userService.deleteUser(user.getId());

         verify(this.userRepository, times(1)).deleteById(user.getId());
    }
    @Test
    @DisplayName("Find all users from the database successfully")
    public void testGetAllUsersFromDb(){
        Page<User> users = mock(Page.class);

        when(this.userRepository.findAll(Mockito.isA(Pageable.class))).thenReturn(users);

        Page<User> result = this.userService.findAllUsers(Mockito.mock(Pageable.class));

        assertEquals(users,result);
    }
    @DisplayName("Find user by email from the database successfully")
    @Test
    public void testFindUserByEmail(){
        User user = UserProvider.userFromDb();

        when(this.userRepository.findByEmail(anyString())).thenReturn(Optional.of(user));

        final User userByEmail = this.userService.findUserByEmail(anyString());

        assertEquals(user, userByEmail);

    }
    @DisplayName("User cannot be found by email")
    @Test
    public void testNoUserFoundByEmail(){

        when(this.userRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> this.userService.findUserByEmail(anyString()));
    }

}
