package com.tsvetkov.rushhour.service.impl.util;

import com.tsvetkov.rushhour.domain.entity.Role;
import com.tsvetkov.rushhour.domain.entity.RoleName;

public class RoleProvider {
    public static Role userRole(){
        Role role = new Role();
        role.setName(RoleName.ROLE_USER);
        role.setId(1);

        return role;
    }

    public static Role adminRole(){
        Role role = new Role();
        role.setName(RoleName.ROLE_ADMIN);
        role.setId(2);

        return role;
    }

}
