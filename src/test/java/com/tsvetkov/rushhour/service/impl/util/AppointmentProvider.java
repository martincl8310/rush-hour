package com.tsvetkov.rushhour.service.impl.util;

import com.tsvetkov.rushhour.domain.entity.Activity;
import com.tsvetkov.rushhour.domain.entity.Appointment;
import com.tsvetkov.rushhour.domain.entity.User;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentPersistDto;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AppointmentProvider {

    public static AppointmentPersistDto appointmentDto(){
        User user = UserProvider.userFromDb();
        Activity activity = ActivityProvider.activityFromDb();

        AppointmentPersistDto appointmentPersistDto = new AppointmentPersistDto();
        appointmentPersistDto.setStartDate(LocalDateTime.parse("2015-08-02T14:00:00"));
       // appointmentPersistDto.setUserId(user.getId());
        appointmentPersistDto.setActivities(List.of(activity.getId()));
        return appointmentPersistDto;
    }

    public static Appointment appointmentFromDb(){
        Set<Activity> activities = new HashSet<>();
        User user = UserProvider.userFromDb();

        Appointment appointmentFromDb = new Appointment();
        appointmentFromDb.setId(1);
        appointmentFromDb.setStartDate(LocalDateTime.parse("2015-08-02T15:00:00"));
        appointmentFromDb.setEndDate(LocalDateTime.parse("2015-08-02T17:00:00"));
        appointmentFromDb.setUser(user);
        appointmentFromDb.setActivities(activities);

        return appointmentFromDb;
    }

    public static Appointment appointmentToPersist(){
        AppointmentPersistDto appointmentPersistDto = AppointmentProvider.appointmentDto();
        User user = UserProvider.userFromDb();
        Set<Activity> activities = new HashSet<>();

        Appointment appointmentToPersist = new Appointment();
        appointmentToPersist.setStartDate(appointmentPersistDto.getStartDate());
        appointmentToPersist.setEndDate(appointmentPersistDto.getStartDate().plusMinutes(10));
        appointmentToPersist.setUser(user);
        appointmentToPersist.setActivities(activities);

        return appointmentToPersist;
    }
}
