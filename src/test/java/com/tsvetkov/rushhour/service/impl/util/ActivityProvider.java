package com.tsvetkov.rushhour.service.impl.util;

import com.tsvetkov.rushhour.domain.entity.Activity;
import com.tsvetkov.rushhour.domain.model.binding.ActivityPersistDto;

import java.math.BigDecimal;
import java.time.Duration;

public class ActivityProvider {

    public static Activity activityFromDb(){
        Activity activityFromDb = new Activity();
        activityFromDb.setPrice(BigDecimal.valueOf(30));
        activityFromDb.setName("test");
        activityFromDb.setDuration(Duration.ofMinutes(30));
        activityFromDb.setId(1);

        return activityFromDb;
    }

    public static ActivityPersistDto activityPersistDto(){
        ActivityPersistDto activityPersistDto = new ActivityPersistDto();
        activityPersistDto.setPrice(1.2);
        activityPersistDto.setName("test");
        activityPersistDto.setDuration(Duration.ofMinutes(30));

        return activityPersistDto;
    }


}
