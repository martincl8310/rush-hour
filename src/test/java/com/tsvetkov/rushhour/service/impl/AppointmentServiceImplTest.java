package com.tsvetkov.rushhour.service.impl;

import com.tsvetkov.rushhour.domain.entity.*;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentActivitiesDto;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentEditDto;
import com.tsvetkov.rushhour.domain.model.binding.AppointmentPersistDto;
import com.tsvetkov.rushhour.exception.customExeption.AccessDeniedException;
import com.tsvetkov.rushhour.exception.customExeption.ConflictException;
import com.tsvetkov.rushhour.exception.customExeption.EntityNotFoundException;
import com.tsvetkov.rushhour.repository.AppointmentRepository;
import com.tsvetkov.rushhour.security.UserPrincipal;
import com.tsvetkov.rushhour.service.impl.util.ActivityProvider;
import com.tsvetkov.rushhour.service.impl.util.AppointmentProvider;
import com.tsvetkov.rushhour.service.impl.util.RoleProvider;
import com.tsvetkov.rushhour.service.impl.util.UserProvider;
import com.tsvetkov.rushhour.util.AuthenticationFacade;
import com.tsvetkov.rushhour.util.validation.DateValidationFactory;
import com.tsvetkov.rushhour.util.validation.DateValidationStrategy;
import com.tsvetkov.rushhour.util.validation.StrategyName;
import com.tsvetkov.rushhour.util.validation.appointmentValidation.EndDateAppointmentValidation;
import jdk.jfr.Description;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AppointmentServiceImplTest {

    @InjectMocks
    private AppointmentServiceImpl appointmentService;

    @Mock
    private AppointmentRepository appointmentRepository;

    @Mock
    private UserServiceImpl userService;

    @Mock
    private ActivityServiceImpl activityService;

    @Mock
    private AuthenticationFacade authenticationFacade;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private DateValidationFactory dateValidationFactory;

    
    @DisplayName("User persist appointment successfully for himself")
    @Test
    public void testPersistedAppointment(){
        AppointmentPersistDto appointmentPersistDto = AppointmentProvider.appointmentDto();
        Activity activity = ActivityProvider.activityFromDb();
        UserPrincipal userPrincipal = UserProvider.userPrincipal();
        User user = UserProvider.userFromDb();
        Appointment appointmentToPersist = AppointmentProvider.appointmentToPersist();

        when(this.activityService.findAllActivitiesById(anyList())).thenReturn(Collections.singletonList(activity));
        when(this.modelMapper.map(appointmentPersistDto, Appointment.class)).thenReturn(appointmentToPersist);
        when(this.userService.findUserById(userPrincipal.getId())).thenReturn(user);
        when(this.appointmentRepository.save(any(Appointment.class))).thenReturn(appointmentToPersist);
        when(this.authenticationFacade.currentAuthenticatedUser()).thenReturn(userPrincipal);
        DateValidationStrategy dateValidationStrategy = Mockito.mock(DateValidationStrategy.class);
        when(this.dateValidationFactory.findStrategy(any())).thenReturn(dateValidationStrategy);

        Appointment result = this.appointmentService.persistAppointment(appointmentPersistDto);
        assertEquals(appointmentToPersist,result);
    }
    @DisplayName("Persist of appointment does not happen if the authenticated user tries to schedule appointment for another user")
    @Test
    public void testUnauthorizedUserRequest(){
        AppointmentPersistDto appointmentPersistDto = AppointmentProvider.appointmentDto();
        User user = UserProvider.userFromDb();
        UserPrincipal userPrincipal = UserProvider.userPrincipal();
        userPrincipal.setId(2);

        when(this.authenticationFacade.currentAuthenticatedUser()).thenReturn(userPrincipal);
        when(this.userService.findUserById(userPrincipal.getId())).thenReturn(user);

        assertThrows(AccessDeniedException.class, () -> this.appointmentService.persistAppointment(appointmentPersistDto));

    }
    @DisplayName("User with ROLE_ADMIN persist appointment successfully")
    @Test
    public void testPersistAppointmentWithRoleAdmin(){
        Role adminRole = RoleProvider.adminRole();
        User user = UserProvider.userFromDb();
        user.getRoles().add(adminRole);
        AppointmentPersistDto appointmentPersistDto = AppointmentProvider.appointmentDto();
        Activity activity = ActivityProvider.activityFromDb();

        Appointment appointment = AppointmentProvider.appointmentToPersist();

        List<GrantedAuthority> authorities = user.getRoles()
                .stream().map(role -> new SimpleGrantedAuthority(role.getName().name()))
                .collect(Collectors.toList());

        UserPrincipal userPrincipal = new UserPrincipal(user.getId(),
                user.getFirstName(), user.getLastName(),user.getEmail(),user.getPassword(), authorities);

        when(this.authenticationFacade.currentAuthenticatedUser()).thenReturn(userPrincipal);
        when(this.userService.findUserById(userPrincipal.getId())).thenReturn(user);
        when(this.activityService.findAllActivitiesById(anyList())).thenReturn(Collections.singletonList(activity));
        when(this.modelMapper.map(appointmentPersistDto, Appointment.class)).thenReturn(appointment);
        when(this.appointmentRepository.save(any(Appointment.class))).thenReturn(appointment);

        DateValidationStrategy dateValidationStrategy = Mockito.mock(DateValidationStrategy.class);
        when(this.dateValidationFactory.findStrategy(any())).thenReturn(dateValidationStrategy);

        Appointment result = this.appointmentService.persistAppointment(appointmentPersistDto);
        assertEquals(appointment,result);

    }
    @DisplayName("Persist of appointment does not happen if startDate is equal to the startDate of existing appointment")
    @Test
    public void testStartDateEqualsOfExistingAppointment(){
        User user = UserProvider.userFromDb();
        Set<Activity> activities = new HashSet<>();
        AppointmentPersistDto appointmentPersistDto = AppointmentProvider.appointmentDto();

        Appointment appointmentToPersist = new Appointment();
        appointmentToPersist.setStartDate(appointmentPersistDto.getStartDate());
        appointmentToPersist.setUser(user);
        appointmentToPersist.setActivities(activities);

        Appointment appointmentFromDb = new Appointment();
        appointmentFromDb.setId(1);
        appointmentFromDb.setStartDate(appointmentPersistDto.getStartDate());
        appointmentFromDb.setUser(user);
        appointmentFromDb.setActivities(activities);

        UserPrincipal userPrincipal = UserProvider.userPrincipal();


        when(this.modelMapper.map(appointmentPersistDto, Appointment.class)).thenReturn(appointmentToPersist);
        when(this.appointmentRepository.findAll()).thenReturn(Collections.singletonList(appointmentFromDb));
        when(this.authenticationFacade.currentAuthenticatedUser()).thenReturn(userPrincipal);
        when(this.userService.findUserById(userPrincipal.getId())).thenReturn(user);
        DateValidationStrategy dateValidationStrategy = Mockito.mock(DateValidationStrategy.class);
        when(this.dateValidationFactory.findStrategy(any())).thenReturn(dateValidationStrategy);
        doThrow(ConflictException.class).when(dateValidationStrategy).validateAppointmentDate(appointmentPersistDto.getStartDate(),
                null, Collections.singletonList(appointmentFromDb));

        assertThrows(ConflictException.class,
                () -> this.appointmentService.persistAppointment(appointmentPersistDto));
         verify(dateValidationStrategy, times(1)).validateAppointmentDate(appointmentPersistDto.getStartDate(),
                null, Collections.singletonList(appointmentFromDb));
         verify(appointmentRepository, never()).save(appointmentToPersist);

    }
    @DisplayName("Persist of appointment does not happen when if startDate is overlapping wth already existing appointment")
    @Test
    public void testStartDateCannotBeBetweenStartDateAndEndDateOfExistingAppointment(){
        User user = UserProvider.userFromDb();
        Set<Activity> activities = new HashSet<>();
        AppointmentPersistDto appointmentPersistDto = AppointmentProvider.appointmentDto();

        Appointment appointmentToPersist = new Appointment();
        appointmentToPersist.setStartDate(appointmentPersistDto.getStartDate());
        appointmentToPersist.setUser(user);
        appointmentToPersist.setActivities(activities);

        Appointment appointment = AppointmentProvider.appointmentFromDb();
        appointment.setStartDate(LocalDateTime.parse("2015-08-02T13:00:00"));
        appointment.setEndDate(LocalDateTime.parse("2015-08-02T15:00:00"));

        UserPrincipal userPrincipal = UserProvider.userPrincipal();

        when(this.modelMapper.map(appointmentPersistDto, Appointment.class)).thenReturn(appointmentToPersist);
        when(this.userService.findUserById(userPrincipal.getId())).thenReturn(user);
        when(this.appointmentRepository.findAll()).thenReturn(Collections.singletonList(appointment));
        when(this.authenticationFacade.currentAuthenticatedUser()).thenReturn(userPrincipal);

        DateValidationStrategy dateValidationStrategy = Mockito.mock(DateValidationStrategy.class);
        when(this.dateValidationFactory.findStrategy(any())).thenReturn(dateValidationStrategy);
        doThrow(ConflictException.class).when(dateValidationStrategy).validateAppointmentDate(appointmentPersistDto.getStartDate(),
                null, Collections.singletonList(appointment));

        assertThrows(ConflictException.class,
                () -> this.appointmentService.persistAppointment(appointmentPersistDto));
        verify(dateValidationStrategy, times(1)).validateAppointmentDate(appointmentPersistDto.getStartDate(),
                null, Collections.singletonList(appointment));
        verify(appointmentRepository, never()).save(appointmentToPersist);
    }
    @DisplayName("Persist of appointment does not happen when endDate is overlapping with already existing appointment")
    @Test
    public void testEndDateOverlapsWithAlreadyExistingAppointment(){
      User user = UserProvider.userFromDb();
      Set<Activity> activities = new HashSet<>();

      Activity activity = ActivityProvider.activityFromDb();
      activity.setDuration(Duration.ofMinutes(120));
      activities.add(activity);

      AppointmentPersistDto appointmentPersistDto = AppointmentProvider.appointmentDto();
      UserPrincipal userPrincipal = UserProvider.userPrincipal();

      Appointment appointmentToPersist = new Appointment();
      appointmentToPersist.setStartDate(appointmentPersistDto.getStartDate());
      appointmentToPersist.setEndDate(appointmentPersistDto.getStartDate()
              .plusMinutes(activity.getDuration().toMinutes()));
      appointmentToPersist.setUser(user);
      appointmentToPersist.setActivities(activities);

      Appointment appointmentFromDb = AppointmentProvider.appointmentFromDb();

      when(this.activityService.findAllActivitiesById(anyList())).thenReturn(Collections.singletonList(activity));
      when(this.modelMapper.map(appointmentPersistDto, Appointment.class)).thenReturn(appointmentToPersist);
      when(this.appointmentRepository.findAll()).thenReturn(Collections.singletonList(appointmentFromDb));
      when(this.userService.findUserById(anyInt())).thenReturn(user);
      when(this.authenticationFacade.currentAuthenticatedUser()).thenReturn(userPrincipal);

      DateValidationStrategy startDateValidationStrategy = Mockito.mock(DateValidationStrategy.class);
      DateValidationStrategy endDateValidationStrategy = Mockito.mock(EndDateAppointmentValidation.class);
      when(this.dateValidationFactory.findStrategy(any())).thenReturn(startDateValidationStrategy);
      when(this.dateValidationFactory.findStrategy(StrategyName.END_DATE_VALIDATION_STRATEGY)).thenReturn(endDateValidationStrategy);

        doThrow(ConflictException.class).when(endDateValidationStrategy).validateAppointmentDate(appointmentToPersist.getEndDate(),
             activity.getDuration(), Collections.singletonList(appointmentFromDb));
        assertThrows(ConflictException.class,
              () -> this.appointmentService.persistAppointment(appointmentPersistDto));
        verify(endDateValidationStrategy, times(1)).validateAppointmentDate(appointmentToPersist.getEndDate(),
                activity.getDuration(), Collections.singletonList(appointmentFromDb));
        verify(this.appointmentRepository, never()).save(appointmentToPersist);
    }
    @DisplayName("Edit startDate of existing appointment successfully")
    @Test
    public void testEditAppointmentSuccessfully(){
        Activity activity = ActivityProvider.activityFromDb();

        AppointmentEditDto appointmentEditDto = new AppointmentEditDto();
        appointmentEditDto.setStartDate(LocalDateTime.parse("2015-08-02T14:30:00"));

        Appointment appointmentFromDb = AppointmentProvider.appointmentFromDb();
        appointmentFromDb.setStartDate(appointmentEditDto.getStartDate());
        appointmentFromDb.setEndDate(appointmentEditDto.getStartDate()
                .plusMinutes(activity.getDuration().toMinutes()));

        UserPrincipal userPrincipal = UserProvider.userPrincipal();

        when(this.authenticationFacade.currentAuthenticatedUser()).thenReturn(userPrincipal);
        when(this.appointmentRepository.findById(anyInt())).thenReturn(Optional.of(appointmentFromDb));
        DateValidationStrategy dateValidationStrategy = Mockito.mock(DateValidationStrategy.class);
        when(this.dateValidationFactory.findStrategy(any())).thenReturn(dateValidationStrategy);
        when(this.appointmentRepository.save(any(Appointment.class))).thenReturn(appointmentFromDb);

        Appointment result = this.appointmentService.editAppointment(1, appointmentEditDto);

        assertEquals(appointmentFromDb, result);

        verify(this.appointmentRepository, times(1)).save(appointmentFromDb);
    }
    @DisplayName("Add successfully new activity to existing appointment")
    @Test
    public void testAddNewActivityToExistingAppointment(){
        List<Activity> activities = new ArrayList<>();

        Activity firstActivity = ActivityProvider.activityFromDb();
        Activity secondActivity = ActivityProvider.activityFromDb();

        activities.add(firstActivity);
        activities.add(secondActivity);

        AppointmentActivitiesDto appointmentActivitiesDto = new AppointmentActivitiesDto();
        appointmentActivitiesDto.setActivities(List.of(1,2));

        Appointment appointmentFromDb = AppointmentProvider.appointmentFromDb();
        appointmentFromDb.setEndDate(appointmentFromDb.getStartDate()
                .plusMinutes(activities.stream().map(Activity::getDuration)
                        .reduce(Duration.ZERO, Duration::plus)
                        .toMinutes()));

        UserPrincipal userPrincipal = UserProvider.userPrincipal();

        when(this.authenticationFacade.currentAuthenticatedUser()).thenReturn(userPrincipal);
        when(this.appointmentRepository.findById(anyInt())).thenReturn(Optional.of(appointmentFromDb));
        when(this.activityService.findAllActivitiesById(anyList())).thenReturn(activities);

        DateValidationStrategy dateValidationStrategy = Mockito.mock(DateValidationStrategy.class);
        when(this.dateValidationFactory.findStrategy(any())).thenReturn(dateValidationStrategy);
        when(this.appointmentRepository.save(any(Appointment.class))).thenReturn(appointmentFromDb);

        Appointment result = this.appointmentService.addActivitiesToAppointment(1, appointmentActivitiesDto);

        assertEquals(appointmentFromDb, result);
    }
    @DisplayName("Cannot add new activity to appointment if endDate overlapping with already existing appointment")
    @Test
    public void testNewEndDateOverlapsWithAnotherAppointment(){
        List<Activity> activities = new ArrayList<>();

        Activity firstActivity = ActivityProvider.activityFromDb();
        Activity secondActivity = ActivityProvider.activityFromDb();

        activities.add(firstActivity);
        activities.add(secondActivity);

        AppointmentActivitiesDto appointmentActivitiesDto = new AppointmentActivitiesDto();

        appointmentActivitiesDto.setActivities(List.of(1,2));

        Appointment appointmentFromDb = AppointmentProvider.appointmentFromDb();
        appointmentFromDb.setEndDate(appointmentFromDb.getStartDate()
                .plusMinutes(activities.stream().map(Activity::getDuration).reduce(Duration.ZERO, Duration::plus).toMinutes()));

        UserPrincipal userPrincipal = UserProvider.userPrincipal();

        when(this.authenticationFacade.currentAuthenticatedUser()).thenReturn(userPrincipal);
        when(this.appointmentRepository.findById(anyInt())).thenReturn(Optional.of(appointmentFromDb));
        when(this.activityService.findAllActivitiesById(anyList())).thenReturn(activities);
        when(this.appointmentRepository.findAll()).thenReturn(Collections.singletonList(appointmentFromDb));

        DateValidationStrategy dateValidationStrategy = Mockito.mock(DateValidationStrategy.class);
        when(this.dateValidationFactory.findStrategy(StrategyName.END_DATE_VALIDATION_FOR_NEW_ACTIVITY)).thenReturn(dateValidationStrategy);

        doThrow(ConflictException.class).when(dateValidationStrategy).validateAppointmentDate(appointmentFromDb.getEndDate(),
                null, Collections.singletonList(appointmentFromDb));

        assertThrows(ConflictException.class, ()-> this.appointmentService.addActivitiesToAppointment(1, appointmentActivitiesDto));
    }
    @DisplayName("Find appointment successfully by appointmentId")
    @Test
    public void testFindAppointmentByAppointmentId(){
        Appointment appointment = AppointmentProvider.appointmentFromDb();

        when(this.appointmentRepository.findById(anyInt())).thenReturn(Optional.of(appointment));

        Appointment result = this.appointmentService.findAppointmentById(appointment.getId());

        assertEquals(appointment, result);

        verify(this.appointmentRepository, times(1)).findById(any());
    }
    @DisplayName("Fail to find appointment by appointmentId and throw EntityNotFoundException")
    @Test
    public void testAppointmentNotFoundById(){

        when(this.appointmentRepository.findById(anyInt())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> this.appointmentService.findAppointmentById(anyInt()));

        verify(this.appointmentRepository, times(1)).findById(any());

    }
    @DisplayName("Delete appointment successfully by appointmentId")
    @Test
    public void testDeleteAppointmentSuccessfully(){
        Appointment appointment = AppointmentProvider.appointmentFromDb();
        UserPrincipal userPrincipal = UserProvider.userPrincipal();

        when(this.authenticationFacade.currentAuthenticatedUser()).thenReturn(userPrincipal);
        when(this.appointmentRepository.findById(anyInt())).thenReturn(Optional.of(appointment));

        this.appointmentService.deleteAppointment(appointment.getId());

        verify(this.appointmentRepository, times(1)).deleteById(appointment.getId());
    }

    @DisplayName("Appointment cannot be deleted if users ids are different")
    @Test
    public void testCannotDeleteAppointmentIfUsersIdsNotMatch(){
        Appointment appointment = AppointmentProvider.appointmentFromDb();
        UserPrincipal userPrincipal = UserProvider.userPrincipal();
        userPrincipal.setId(2);
        when(this.authenticationFacade.currentAuthenticatedUser()).thenReturn(userPrincipal);

        when(this.appointmentRepository.findById(anyInt())).thenReturn(Optional.of(appointment));

        assertThrows(AccessDeniedException.class, () -> this.appointmentService.deleteAppointment(appointment.getId()));
    }
    @DisplayName("Find all appointments successfully")
    @Test
    public void testFindAllAppointments(){
        User user = UserProvider.userFromDb();
        Appointment appointment = AppointmentProvider.appointmentFromDb();
        UserPrincipal userPrincipal = UserProvider.userPrincipal();
        when(this.authenticationFacade.currentAuthenticatedUser()).thenReturn(userPrincipal);

        when(this.appointmentRepository.findAllByUserId(user.getId())).thenReturn(Collections.singletonList(appointment));

        final List<Appointment> result = this.appointmentService.findAllAppointments();

        assertEquals(List.of(appointment), result);

    }


}